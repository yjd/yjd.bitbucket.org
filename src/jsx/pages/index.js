//Must explicity export all pages.
export {default as GalleryPage} from './gallery.jsx';
export {default as MandelbrotPage} from './mandelbrot.jsx';
export {default as BuddhabrotPage} from './buddhabrot.jsx';
